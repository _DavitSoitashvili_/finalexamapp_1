package davit.soitashvili.demo.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String pdfUrl;

    @OneToOne(mappedBy = "attachment", cascade = CascadeType.ALL)
    private Book book;
}
